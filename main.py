import numpy as np
import json
from matplotlib import pyplot as plt


class Measurement:
    def __init__(self,json):
        self.json = json
        self.json_opener()
        self.json_converter()
        self.json_plot()

    def json_opener(self):
        self.opener = open(self.json, )
        self.dog = json.load(self.opener)
        self.data = self.dog["data"]

    def json_converter(self):
        from datetime import datetime
        if "gravity" not in self.data:
            print("Warning! No gravity data in file!")
            self.data["gravity"] = []
        else:
            self.gravity = self.data["gravity"]
            self.gravity = [word for line in self.gravity for word in line.split()]
            self.gravity = [float(i) for i in self.gravity]
            self.s1 = len(self.gravity)
            self.gravity_array = np.array(self.gravity)
            self.gravity_array = np.hsplit(self.gravity_array, self.s1 / 4)
            self.gravity_array = np.stack(self.gravity_array, axis=1)
            self.gravity_time = self.gravity_array[0]
            self.gravity_time_first = datetime.fromtimestamp(self.gravity_time[0])
            self.gravity_time_last = datetime.fromtimestamp(self.gravity_time[-1])
            self.gravity_ellapsed = self.gravity_time_last - self.gravity_time_first
            self.gravity_ellapsed = self.gravity_ellapsed.total_seconds()
            self.sample_rate_gravity_1 = len(self.gravity_array[1])/self.gravity_ellapsed
            self.sample_rate_gravity_2 = len(self.gravity_array[2])/self.gravity_ellapsed
            self.sample_rate_gravity_3 = len(self.gravity_array[3])/self.gravity_ellapsed
        #
        if "attitude" not in self.data:
            print("Warning! No attitude data in file!")
            self.data["attitude"] = []
        else:
            self.attitude = self.data["attitude"]
            self.attitude = [word for line in self.attitude for word in line.split()]
            self.attitude = [float(i) for i in self.attitude]
            self.s2 = len(self.attitude)
            self.attitude_array = np.array(self.attitude)
            self.attitude_array = np.hsplit(self.attitude_array, self.s2 / 5)
            self.attitude_array = np.stack(self.attitude_array, axis=1)
            self.attitude_time = self.attitude_array[0]
            self.attitude_time_first = datetime.fromtimestamp(self.attitude_time[0])
            self.attitude_time_last = datetime.fromtimestamp(self.attitude_time[-1])
            self.attitude_ellapsed = self.attitude_time_last - self.attitude_time_first
            self.attitude_ellapsed = self.attitude_ellapsed.total_seconds()
            self.sample_rate_attitude_1 = len(self.attitude_array[1])/self.attitude_ellapsed
            self.sample_rate_attitude_2 = len(self.attitude_array[2])/self.attitude_ellapsed
            self.sample_rate_attitude_3 = len(self.attitude_array[3])/self.attitude_ellapsed
            self.sample_rate_attitude_4 = len(self.attitude_array[4])/self.attitude_ellapsed
        #
        if "user_acceleration" not in self.data:
            print("Warning! No user acceleration data in file!")
            self.data["user_acceleration"] = []
        else:
            self.user_acceleration = self.data["user_acceleration"]
            self.user_acceleration = [word for line in self.user_acceleration for word in line.split()]
            self.user_acceleration = [float(i) for i in self.user_acceleration]
            self.s3 = len(self.user_acceleration)
            self.user_acceleration_array = np.array(self.user_acceleration)
            self.user_acceleration_array = np.hsplit(self.user_acceleration_array, self.s3 / 4)
            self.user_acceleration_array = np.stack(self.user_acceleration_array, axis=1)
            self.user_acceleration_time = self.user_acceleration_array[0]
            self.user_acceleration_time_first = datetime.fromtimestamp(self.user_acceleration_time[0])
            self.user_acceleration_time_last = datetime.fromtimestamp(self.user_acceleration_time[-1])
            self.user_acceleration_ellapsed = self.user_acceleration_time_last - self.user_acceleration_time_first
            self.user_acceleration_ellapsed = self.user_acceleration_ellapsed.total_seconds()
            self.sample_rate_user_acceleration_1 = len(self.user_acceleration_array[1])/self.user_acceleration_ellapsed
            self.sample_rate_user_acceleration_2 = len(self.user_acceleration_array[2])/self.user_acceleration_ellapsed
            self.sample_rate_user_acceleration_3 = len(self.user_acceleration_array[3])/self.user_acceleration_ellapsed
        #
        if "rotation_rate" not in self.data:
            print("Warning! No rotationrate data in file!")
            self.data["rotation_rate"] = []
        else:
            self.rotation_rate = self.data["rotation_rate"]
            self.rotation_rate = [word for line in self.rotation_rate for word in line.split()]
            self.rotation_rate = [float(i) for i in self.rotation_rate]
            self.s4 = len(self.rotation_rate)
            self.rotation_rate_array = np.array(self.rotation_rate)
            self.rotation_rate_array = np.hsplit(self.rotation_rate_array, self.s4 / 4)
            self.rotation_rate_array = np.stack(self.rotation_rate_array, axis=1)
            self.rotation_rate_time = self.rotation_rate_array[0]
            self.rotation_rate_time_first = datetime.fromtimestamp(self.rotation_rate_time[0])
            self.rotation_rate_time_last = datetime.fromtimestamp(self.rotation_rate_time[-1])
            self.rotation_rate_ellapsed = self.rotation_rate_time_last - self.rotation_rate_time_first
            self.rotation_rate_ellapsed = self.rotation_rate_ellapsed.total_seconds()
            self.sample_rate_rotation_rate_1 = len(self.rotation_rate_array[1])/self.rotation_rate_ellapsed
            self.sample_rate_rotation_rate_2 = len(self.rotation_rate_array[2])/self.rotation_rate_ellapsed
            self.sample_rate_rotation_rate_3 = len(self.rotation_rate_array[3])/self.rotation_rate_ellapsed
        if self.data["gravity"] == [] or self.data["attitude"] or self.data["user_acceleration"] or self.data["rotation_rate"]:
            pass
        else:
            if self.s1/4 != self.s2/5 or self.s1/4 != self.s3/4 or self.s1/4 != self.s4/4:
                raise ValueError("Not enough data")
            if self.s2/5 != self.s3/4 or self.s4/4 != self.s3/4:
                raise ValueError("Not enough data")
            if self.s3/4 != self.s4/4:
                raise ValueError("Not enough data")
    def json_plot(self):
        if self.data["gravity"] == []:
            pass
        else:
            fig1, axs1 = plt.subplots(2, 2)
            fig1.suptitle('Gravity')
            axs1[0, 0].plot(self.gravity_array[0], self.gravity_array[1])
            axs1[0, 1].plot(self.gravity_array[0], self.gravity_array[2])
            axs1[1, 0].plot(self.gravity_array[0], self.gravity_array[3])
            plt.savefig("figure1.png")
        
        if self.data["attitude"] == []:
            pass
        else:
            fig2, axs2 = plt.subplots(2, 3)
            fig2.suptitle('Attitude')
            axs2[0, 0].plot(self.attitude_array[0], self.attitude_array[1])
            axs2[0, 1].plot(self.attitude_array[0], self.attitude_array[2])
            axs2[1, 0].plot(self.attitude_array[0], self.attitude_array[3])
            axs2[1, 1].plot(self.attitude_array[0], self.attitude_array[4])
            plt.savefig("figure2.png")
        
        if self.data["user_acceleration"] == []:
            pass
        else:
            fig3, axs3 = plt.subplots(2, 2)
            fig3.suptitle('User acceleration')
            axs3[0, 0].plot(self.user_acceleration_array[0],self.user_acceleration_array[1])
            axs3[0, 1].plot(self.user_acceleration_array[0], self.user_acceleration_array[2])
            axs3[1, 0].plot(self.user_acceleration_array[0], self.user_acceleration_array[3])
            plt.savefig("figure3.png")
        
        if self.data["rotation_rate"] == []:
            pass
        else:
            fig4, axs4 = plt.subplots(2, 2)
            fig4.suptitle('Rotation rate')
            axs4[0, 0].plot(self.rotation_rate_array[0], self.rotation_rate_array[1])
            axs4[0, 1].plot(self.rotation_rate_array[0], self.rotation_rate_array[2])
            axs4[1, 0].plot(self.rotation_rate_array[0], self.rotation_rate_array[3])
            plt.savefig("figure4.png")

        plt.show()



path = 'SensDog_1_Tasli_2016-10-23_16_16_30.607.json'

if __name__ == '__main__':
    Measurement(path)
